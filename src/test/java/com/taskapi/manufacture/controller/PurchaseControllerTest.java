package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.service.PurchaseService;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class PurchaseControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PurchaseService purchaseService;

    @InjectMocks
    private PurchaseController purchaseController;

    @Before
    public void init() throws Exception{
        mockMvc = MockMvcBuilders.standaloneSetup(purchaseController).build();
    }

    @Test
    public void addPurchaseTest() throws Exception{
        String jsonBody =
                "{\n" +
                        "  \"createdDate\": \"2019-05-31\",\n" +
                        "  \"customerId\": 1,\n" +
                        "  \"qtyPurchase\": 10,\n" +
                        "  \"stockId\": 1\n" +
                        "}";
        log.info(jsonBody);
        mockMvc.perform(post("/purchase")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : addPurchase")))
                .andExpect(jsonPath("$.message", Matchers.is("Purchasing Success !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }

    @Test
    public void findPurchaseByIdTest() throws Exception{
        int purchaseId = 1;
        mockMvc.perform(get("/purchase/" + purchaseId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : findPurchaseById")))
                .andExpect(jsonPath("$.message", Matchers.is("Successfully retrieve by Purchase ID : " + purchaseId)))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }

    @Test
    public void findAllPurchaseTest() throws Exception{
        mockMvc.perform(get("/purchase")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : findAllPurchase")))
                .andExpect(jsonPath("$.message", Matchers.is("Data retrieved !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }

}
