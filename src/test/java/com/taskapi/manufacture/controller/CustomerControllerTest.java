package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class CustomerControllerTest {

    private MockMvc mockMvc;

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    @Before
    public void init() throws Exception{
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
    }

    @Test
    public void addCustomerTest() throws Exception{
        String jsonBody =
                "\n{\n" +
                        "  \"accountBalance\": 30000000,\n" +
                        "  \"customerName\": \"PT. ABC\",\n" +
                        "  \"status\": \"Aktif\"\n" +
                        "}";
        log.info(jsonBody);
        mockMvc.perform(post("/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : addCustomer")))
                .andExpect(jsonPath("$.message", Matchers.is("Add Customer Success !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }

    @Test
    public void updateCustomerTest() throws Exception{
        int customerId = 1;
        String jsonBody =
                "\n{\n" +
                        "  \"accountBalance\": 540000000,\n" +
                        "  \"customerName\": \"PT. ABC\",\n" +
                        "  \"status\": \"Non Aktif\"\n" +
                        "}";
        mockMvc.perform(put("/customer/"  + customerId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : updateCustomer")))
                .andExpect(jsonPath("$.message", Matchers.is("Data with ID : " + customerId + " updated !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));
    }

    @Test
    public void findCustomerByIdTest() throws Exception{
        int customerId = 1;
        mockMvc.perform(get("/customer/" + customerId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : findCustomerById")))
                .andExpect(jsonPath("$.message", Matchers.is("Successfully retrieve by Customer ID : " + customerId)))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }

    @Test
    public void findAllCustomerTest() throws Exception{
        mockMvc.perform(get("/customer")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : findAllCustomer")))
                .andExpect(jsonPath("$.message", Matchers.is("Data retrieved !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));


    }

    @Test
    public void deleteCustomerByIdTest() throws Exception{
        int customerId = 1;
        mockMvc.perform(delete("/customer/" + customerId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : deleteCustomerById")))
                .andExpect(jsonPath("$.message", Matchers.is("Data with ID " + customerId + " deleted !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }


}
