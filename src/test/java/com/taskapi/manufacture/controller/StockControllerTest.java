package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class StockControllerTest {

    private MockMvc mockMvc;

    @Mock
    private StockService stockService;

    @InjectMocks
    private StockController stockController;

    @Before
    public void init() throws Exception{
        mockMvc = MockMvcBuilders.standaloneSetup(stockController).build();
    }

    @Test
    public void addStockTest() throws Exception{
        String jsonBody =
                "{\n" +
                        "  \"measurementUnit\": \"PCS\",\n" +
                        "  \"name\": \"Macbook Pro 2018\",\n" +
                        "  \"qtyStock\": 54,\n" +
                        "  \"status\": \"New\",\n" +
                        "  \"storageLocation\": \"JKT\",\n" +
                        "  \"unitPrice\": 14000000\n" +
                        "}";
        log.info(jsonBody);
        mockMvc.perform(post("/stock")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : addStock")))
                .andExpect(jsonPath("$.message", Matchers.is("Successfully add Stock !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));
    }

    @Test
    public void updateStockTest() throws Exception{
        int stockId = 1;
        String jsonBody =
                "{\n" +
                        "  \"measurementUnit\": \"PCS\",\n" +
                        "  \"name\": \"Macbook Pro 2018\",\n" +
                        "  \"qtyStock\": 77,\n" +
                        "  \"status\": \"New\",\n" +
                        "  \"storageLocation\": \"KRW\",\n" +
                        "  \"unitPrice\": 14500000\n" +
                        "}";
        mockMvc.perform(put("/stock/"  + stockId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : updateStock")))
                .andExpect(jsonPath("$.message", Matchers.is("Data with ID : " + stockId + " updated !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));
    }

    @Test
    public void getStockByIdTest() throws Exception{
        int stockId = 1;
        mockMvc.perform(get("/stock/" + stockId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : getStockById")))
                .andExpect(jsonPath("$.message", Matchers.is("Successfully retrieve by Stock ID : " + stockId)))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }

    @Test
    public void findAllStockTest() throws Exception{
        mockMvc.perform(get("/stock")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : findAllStock")))
                .andExpect(jsonPath("$.message", Matchers.is("Data retrieved !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));


    }

    @Test
    public void deleteStockByIdTest() throws Exception{
        int stockId = 1;
        mockMvc.perform(delete("/stock/" + stockId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.service", Matchers.is("Controller method loc : deleteStockById")))
                .andExpect(jsonPath("$.message", Matchers.is("Data with ID " + stockId + " deleted !")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)));

    }

}
