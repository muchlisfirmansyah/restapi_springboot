package com.taskapi.manufacture.service;

import com.taskapi.manufacture.model.dto.PurchaseReqDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class PurchaseServiceTest {
    @Autowired
    PurchaseService purchaseService;

    @Test
    public void findAll(){
        List<PurchaseReqDto> purchaseReqDtoList = purchaseService.findAll();
        log.info("---List Purchase--");
        for (int i = 0; i < purchaseReqDtoList.size();i++){
            log.info("Purchase Id  : " + purchaseReqDtoList.get(i).getPurchaseId());
            log.info("Customer Id  : " + purchaseReqDtoList.get(i).getCustomerId());
            log.info("Stock Id     : " + purchaseReqDtoList.get(i).getStockId());
            log.info("Qty Purchase : " + purchaseReqDtoList.get(i).getQtyPurchase());
            log.info("Total Price  : " + purchaseReqDtoList.get(i).getTotalPrice());
            log.info("Created Date : " + purchaseReqDtoList.get(i).getCreatedDate());

            assertThat(purchaseReqDtoList.size()).isNotNull();
        }
    }

    @Test
    public void findById(){
        long Id = 1;
        PurchaseReqDto purchaseReqDto = purchaseService.findById(Id);
        log.info("Purchase Id  : " + purchaseReqDto.getPurchaseId());
        log.info("Customer Id  : " + purchaseReqDto.getCustomerId());
        log.info("Stock Id     : " + purchaseReqDto.getStockId());
        log.info("Qty Purchase : " + purchaseReqDto.getQtyPurchase());
        log.info("Total Price  : " + purchaseReqDto.getTotalPrice());
        log.info("Created Date : " + purchaseReqDto.getCreatedDate());

        assertThat(purchaseReqDto.getPurchaseId())
                .isEqualTo(Id);
    }

}
