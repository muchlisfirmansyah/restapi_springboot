package com.taskapi.manufacture.service;

import com.taskapi.manufacture.model.dto.CustomerReqDto;
import com.taskapi.manufacture.model.dto.CustomerRespDto;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class CustomerServiceTest {
    @Autowired
    CustomerService customerService;


    @Test
    public void findAll(){
        List<CustomerReqDto> customer = customerService.findAll();
        assertThat(customer.size()).isNotNull();
        log.info("---List Customer--");
        for (int i = 0; i < customer.size();i++){
            log.info("Id : " + (i+1));
            log.info("Customer Name : " + customer.get(i).getCustomerName());
            log.info("Account Balance : " + customer.get(i).getAccountBalance());
        }
    }

    @Test
    @Transactional
    public void findById(){
        long Id = 1;
        CustomerReqDto customerReqDto = customerService.findById(Id);
        log.info("Customer ID     : " + customerReqDto.getCustomerId());
        log.info("Customer Name   : " + customerReqDto.getCustomerName());
        log.info("Account Balance : " + customerReqDto.getAccountBalance());
        log.info("Status          : " + customerReqDto.getStatus());

        assertThat(customerReqDto.getCustomerId())
                .isEqualTo(Id);
    }

    @Test
    public void create(){
        CustomerRespDto customerRespDto = new CustomerRespDto();
        String customerName  = "PT. DOKU";
        double accountBalance = 350000000;
        String status = "AKTIF";

        customerRespDto.setCustomerName(customerName);
        customerRespDto.setAccountBalance(accountBalance);
        customerRespDto.setStatus(status);
        log.info("Customer Name   : " + customerRespDto.getCustomerName());
        log.info("Account Balance : " + customerRespDto.getAccountBalance());
        log.info("Status          : " + customerRespDto.getStatus());

        CustomerRespDto customerRespDtoCreate = customerService.create(customerRespDto);
        assertThat(customerRespDtoCreate.getCustomerName())
                .isEqualTo(customerName);
        assertThat(customerRespDtoCreate.getAccountBalance())
                .isEqualTo(accountBalance);
        assertThat(customerRespDtoCreate.getStatus())
                .isEqualTo(status);
    }

    @Test
    @Transactional
    public void update(){

        CustomerRespDto customerRespDto = new CustomerRespDto();
        long Id = 2;
        CustomerReqDto customerReqDto = customerService.findById(Id);

        double accountBalance = 450000000;
        log.info("accountBalance (BEFORE) :" + customerReqDto.getAccountBalance());
        customerRespDto.setAccountBalance(accountBalance);
        CustomerRespDto customerRespDtoUpdate = customerService.update(Id, customerRespDto);
        log.info("accountBalance (AFTER) : " + customerRespDtoUpdate.getAccountBalance());
        assertThat(customerRespDtoUpdate.getAccountBalance())
                .isEqualTo(accountBalance);
    }

    @Test
    public void delete(){
        long Id = 3;
        log.info("BEFORE ----");
        List<CustomerReqDto> customerReqDtoList = customerService.findAll();
        log.info("size = " + customerReqDtoList.size());
        customerService.delete(Id);
        log.info("Data deleted");
        log.info("AFTER ----");
        List<CustomerReqDto> customerReqDtoListAfter = customerService.findAll();
        log.info("size = " + customerReqDtoListAfter.size());
        assertThat(customerReqDtoList.size())
                .isGreaterThan(customerReqDtoListAfter.size());
    }

}
