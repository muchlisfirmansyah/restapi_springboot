package com.taskapi.manufacture.service;

import com.taskapi.manufacture.model.dto.StockReqDto;
import com.taskapi.manufacture.model.dto.StockRespDto;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class StockServiceTest {
    @Autowired
    StockService stockService;

    @Test
    public void findAll(){
        List<StockReqDto> stockReqDtoList = stockService.findAll();
        assertThat(stockReqDtoList.size()).isNotNull();
        log.info("---List Stock--");
        for (int i = 0; i < stockReqDtoList.size();i++){
            log.info("Stock Id     : " + stockReqDtoList.get(i).getStockId());
            log.info("Stock Name   : " + stockReqDtoList.get(i).getName());
            log.info("Qty Stock    : " + stockReqDtoList.get(i).getQtyStock());
            log.info("Measure Unit : " + stockReqDtoList.get(i).getMeasurementUnit());
            log.info("Unit Price   : " + stockReqDtoList.get(i).getUnitPrice());
            log.info("Status       : " + stockReqDtoList.get(i).getStatus());
            log.info("Storage Loc  : " + stockReqDtoList.get(i).getStorageLocation());

        }
    }

    @Test
    @Transactional
    public void findById(){
        int Id = 1;
        StockReqDto stockReqDto = stockService.findById(new Long(Id));
        log.info("Stock Id     : " + stockReqDto.getStockId());
        log.info("Stock Name   : " + stockReqDto.getName());
        log.info("Qty Stock    : " + stockReqDto.getQtyStock());
        log.info("Measure Unit : " + stockReqDto.getMeasurementUnit());
        log.info("Unit Price   : " + stockReqDto.getUnitPrice());
        log.info("Status       : " + stockReqDto.getStatus());
        log.info("Storage Loc  : " + stockReqDto.getStorageLocation());


        assertThat(stockReqDto.getStockId())
                .isEqualTo(Id);
    }

    @Test
    public void create(){
        StockRespDto stockRespDto = new StockRespDto();

        String stockName  = "I Pad Pro";
        int qtyStock = 44;
        String measureUnit = "PCS";
        double unitPrice = 12000000;
        String storageLoc = "JKT";
        String status = "New";

        stockRespDto.setName(stockName);
        stockRespDto.setQtyStock(qtyStock);
        stockRespDto.setMeasurementUnit(measureUnit);
        stockRespDto.setUnitPrice(unitPrice);
        stockRespDto.setStorageLocation(storageLoc);
        stockRespDto.setStatus(status);

        StockRespDto stockRespDtoCreate = stockService.create(stockRespDto);
        assertThat(stockRespDtoCreate.getName())
                .isEqualTo(stockName);
        assertThat(stockRespDtoCreate.getQtyStock())
                .isEqualTo(qtyStock);
        assertThat(stockRespDtoCreate.getMeasurementUnit())
                .isEqualTo(measureUnit);
        assertThat(stockRespDtoCreate.getUnitPrice())
                .isEqualTo(unitPrice);
        assertThat(stockRespDtoCreate.getStorageLocation())
                .isEqualTo(storageLoc);
        assertThat(stockRespDtoCreate.getStatus())
                .isEqualTo(status);
    }

    @Test
    @Transactional
    public void update(){
        StockRespDto stockRespDto = new StockRespDto();
        long Id = 2;
        StockReqDto stockReqDto = stockService.findById(Id);

        int qtyStock = 430;
        log.info("Quantity Stock (BEFORE) :" + stockReqDto.getQtyStock());
        stockRespDto.setQtyStock(qtyStock);
        StockRespDto stockRespDtoUpdate = stockService.update(Id, stockRespDto);
        log.info("Quantity Stiock (AFTER) : " + stockRespDtoUpdate.getQtyStock());
        assertThat(stockRespDtoUpdate.getQtyStock())
                .isEqualTo(qtyStock);
    }

    @Test
    @Transactional
    public void delete(){
        long Id = 3;
        log.info("BEFORE ----");
        List<StockReqDto> stockReqDtoList = stockService.findAll();
        log.info("size = " + stockReqDtoList.size());
        stockService.delete(Id);
        log.info("Data deleted");
        log.info("AFTER ----");
        List<StockReqDto> stockReqDtoListAfter = stockService.findAll();
        log.info("size = " + stockReqDtoListAfter.size());
        assertThat(stockReqDtoList.size())
                .isGreaterThan(stockReqDtoListAfter.size());
    }

}
