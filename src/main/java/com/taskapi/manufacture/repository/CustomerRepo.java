package com.taskapi.manufacture.repository;

import com.taskapi.manufacture.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface CustomerRepo extends JpaRepository<Customer, Long> {

}
