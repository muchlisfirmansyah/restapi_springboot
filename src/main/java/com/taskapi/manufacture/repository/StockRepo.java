package com.taskapi.manufacture.repository;

import com.taskapi.manufacture.model.Stock;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface StockRepo extends JpaRepository<Stock, Long> {

}