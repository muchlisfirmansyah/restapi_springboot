package com.taskapi.manufacture.repository;

import com.taskapi.manufacture.model.Purchase;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is a Javadoc comment
 */
public interface PurchaseRepo extends JpaRepository<Purchase, Long> {

}