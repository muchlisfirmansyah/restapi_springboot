package com.taskapi.manufacture.util;

import lombok.Getter;
import lombok.Setter;

/*Generic Method*/
@Setter /* Setter */
@Getter /* Getter */

/**
 * This is a Javadoc comment
 * Response message
 * @param <T> data
 */
public class Response<T> {
    private String service;
    private String message;
    private T data;
}