package com.taskapi.manufacture.service;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

import com.taskapi.manufacture.model.dto.StockReqDto;
import com.taskapi.manufacture.model.dto.StockRespDto;
import com.taskapi.manufacture.model.Stock;
import com.taskapi.manufacture.exception.RecordNotFound;
import com.taskapi.manufacture.model.mapper.StockReqMapper;
import com.taskapi.manufacture.model.mapper.StockRespMapper;
import com.taskapi.manufacture.repository.StockRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * This is a Javadoc comment
 */
@Service
public class StockService {
    private static final String STOCK_NOT_FOUND = "Stock ID : ";
    @Autowired
    StockRepo stockRepo;

    @Autowired
    StockReqMapper stockReqMapper;

    @Autowired
    StockRespMapper stockRespMapper;

    public List<StockReqDto> findAll() {
        List<Stock> stockList = stockRepo.findAll();
        List<StockReqDto> stockReqDtoList = new ArrayList<>();
        for(Stock stock : stockList){
            StockReqDto stockReqDto = stockReqMapper.map(stock, StockReqDto.class);
            stockReqDtoList.add(stockReqDto);

        }
        return stockReqDtoList;
    }

    public StockReqDto findById(Long stockId) {

        Optional<Stock>optionalStock = stockRepo.findById(stockId);
        if (!optionalStock.isPresent())
            throw new RecordNotFound(STOCK_NOT_FOUND + stockId);

        Stock stock = stockRepo.getOne(stockId);
        return stockReqMapper.map(stock, StockReqDto.class);
    }

    public StockRespDto update(Long stockId, StockRespDto stockRespDto) {
        Optional<Stock>optionalStock = stockRepo.findById(stockId);
        if (!optionalStock.isPresent())
            throw new RecordNotFound(STOCK_NOT_FOUND + stockId);

        Stock stock = new Stock();

        stock.setMeasurementUnit(stockRespDto.getMeasurementUnit());
        stock.setName(stockRespDto.getName());
        stock.setQtyStock(stockRespDto.getQtyStock());
        stock.setStatus(stockRespDto.getStatus());
        stock.setStorageLocation(stockRespDto.getStorageLocation());
        stock.setUnitPrice(stockRespDto.getUnitPrice());
        stockRepo.save(stock);

        return stockRespDto;
    }

    public StockRespDto create(StockRespDto stockRespDto) {

        Stock stock = stockRespMapper.map(stockRespDto, Stock.class);
        Stock stockSaved = stockRepo.save(stock);

        return stockRespMapper.map(stockSaved, StockRespDto.class);
    }

    public void delete(Long stockId) {
        Optional<Stock>optionalStock = stockRepo.findById(stockId);
        if (!optionalStock.isPresent())
            throw new RecordNotFound(STOCK_NOT_FOUND + stockId);

        stockRepo.deleteById(stockId);
    }

}