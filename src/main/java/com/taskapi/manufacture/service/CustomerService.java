package com.taskapi.manufacture.service;

import com.taskapi.manufacture.model.dto.CustomerReqDto;
import com.taskapi.manufacture.model.dto.CustomerRespDto;
import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.exception.RecordNotFound;
import com.taskapi.manufacture.model.mapper.CustomerReqMapper;
import com.taskapi.manufacture.model.mapper.CustomerRespMapper;
import com.taskapi.manufacture.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This is a Javadoc comment
 */
@Service
public class CustomerService {
    private static final String CUSTOMER_NOT_FOUND = "Customer ID : ";
    @Autowired
    CustomerRepo customerRepo;

    @Autowired
    CustomerReqMapper customerReqMapper;

    @Autowired
    CustomerRespMapper customerRespMapper;

    /**
     * @Service
     * This is a Javadoc comment
     * Get Data from Customer
     * @return customerReqDtoList
     */
    public List<CustomerReqDto> findAll() {
        List<Customer> customerList = customerRepo.findAll();
        List<CustomerReqDto> customerReqDtoList = new ArrayList<>();
        for(Customer customer : customerList){
            CustomerReqDto customerReqDto = customerReqMapper.map(customer, CustomerReqDto.class);
            customerReqDtoList.add(customerReqDto);
        }
        return customerReqDtoList;
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Get Data by customerId from Customer
     * @param customerId
     * @return customerReqDto
     */
    public CustomerReqDto findById(Long customerId) {
        Optional<Customer> optionalCustomer = customerRepo.findById(customerId);
        if (!optionalCustomer.isPresent())
            throw new RecordNotFound(CUSTOMER_NOT_FOUND + customerId);

        Customer customer = customerRepo.getOne(customerId);
        return customerReqMapper.map(customer, CustomerReqDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Update Customer
     * @param customerId, customerRespDto
     * @return customerRespDto
     */
    public CustomerRespDto update(Long customerId, CustomerRespDto customerRespDto) {
        Optional<Customer> optionalCustomer = customerRepo.findById(customerId);
        if (!optionalCustomer.isPresent())
            throw new RecordNotFound(CUSTOMER_NOT_FOUND + customerId);

        Customer customerExist = customerRepo.getOne(customerId);

        Customer customer = customerRespMapper.map(customerRespDto, Customer.class);
        Customer customerSaved = customerRepo.save(customer);

        return customerRespMapper.map(customerSaved, CustomerRespDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Create Payment
     * @param customerRespDto
     * @return customerRespMapper
     */
    public CustomerRespDto create(CustomerRespDto customerRespDto) {
        Customer customer = customerRespMapper.map(customerRespDto, Customer.class);
        Customer customerSaved = customerRepo.save(customer);

        return customerRespMapper.map(customerSaved, CustomerRespDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Delete Customer
     * @param customerId
     */
    public void delete(Long customerId) {
        Optional<Customer> optionalCustomer = customerRepo.findById(customerId);
        if (!optionalCustomer.isPresent())
            throw new RecordNotFound(CUSTOMER_NOT_FOUND + customerId);

        customerRepo.deleteById(customerId);
    }

}
