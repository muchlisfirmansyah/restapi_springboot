package com.taskapi.manufacture.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.taskapi.manufacture.model.dto.*;
import com.taskapi.manufacture.model.Purchase;
import com.taskapi.manufacture.exception.RecordNotFound;
import com.taskapi.manufacture.model.mapper.PurchaseReqMapper;
import com.taskapi.manufacture.model.mapper.PurchaseRespMapper;
import com.taskapi.manufacture.repository.PurchaseRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * This is a Javadoc comment
 */
@Service
public class PurchaseService {

    @Autowired
    PurchaseRepo purchaseRepo;

    @Autowired
    StockService stockService;

    @Autowired
    CustomerService customerService;

    @Autowired
    PurchaseReqMapper purchaseReqMapper;

    @Autowired
    PurchaseRespMapper purchaseRespMapper;



    public List<PurchaseReqDto> findAll() {
        List<Purchase> purchaseList = purchaseRepo.findAll();
        List<PurchaseReqDto> purchaseReqDtoList = new ArrayList<>();
        for (Purchase purchase : purchaseList) {

            PurchaseReqDto purchaseReqDto = purchaseReqMapper.map(purchase, PurchaseReqDto.class);
            purchaseReqDtoList.add(purchaseReqDto);
        }
        return purchaseReqDtoList;
    }


    public PurchaseReqDto findById(Long purchaseId) {
        Optional<Purchase>optionalPurchase = purchaseRepo.findById(purchaseId);
        if (!optionalPurchase.isPresent())
            throw new RecordNotFound("Purchase ID : " + purchaseId + " Not Found");

        Purchase purchase = purchaseRepo.getOne(purchaseId);
        return purchaseReqMapper.map(purchase, PurchaseReqDto.class);
    }

    public PurchaseRespDto create(PurchaseRespDto purchaseRespDto) {

        StockReqDto stockReqDto = stockService.findById(purchaseRespDto.getStockId());

        double getUnitPrice = stockReqDto.getUnitPrice();
        int getQtyPurchase = purchaseRespDto.getQtyPurchase();
        double totalPrice = getUnitPrice * getQtyPurchase;
        purchaseRespDto.setTotalPrice(totalPrice);

        //Update Stock
        int qtyStock = stockReqDto.getQtyStock();
        int stockNew = qtyStock - getQtyPurchase;
        if (stockNew < 0) {
            throw new IllegalArgumentException("Stock purchases exceed the available inventory, Purchase couldn't be created");
        }
        stockReqDto.setQtyStock(stockNew);

        //Update Balance
        CustomerReqDto customerReqDto = customerService.findById(purchaseRespDto.getCustomerId());
        double balanceCurr = customerReqDto.getAccountBalance();
        double balanceNew = balanceCurr - totalPrice;
        if (balanceNew < 0) {
            throw new IllegalArgumentException("Insufficient account balance to make a purchase, Purchase couldn't be created");
        }
        customerReqDto.setAccountBalance(balanceNew);

        Purchase purchase = new Purchase();

        purchase.setCreatedDate(purchaseRespDto.getCreatedDate());
        purchase.setCustomerId(purchaseRespDto.getCustomerId());
        purchase.setStockId(purchaseRespDto.getStockId());
        purchase.setQtyPurchase(purchaseRespDto.getQtyPurchase());
        purchase.setTotalPrice(purchaseRespDto.getTotalPrice());

        purchaseRepo.save(purchase);
        return purchaseRespDto;

    }
}
