package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.StockRespDto;

import com.taskapi.manufacture.service.StockService;
import com.taskapi.manufacture.util.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * This is a Javadoc comment
 */
@RestController
@RequestMapping(value = "stock")
public class StockController {
    @Autowired
    StockService stockService;

    @PostMapping
    ResponseEntity<Response> addStock(@RequestBody @Valid StockRespDto stockDto) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Successfully add Stock !");
        response.setData(stockService.create(stockDto));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }


    @GetMapping
    ResponseEntity<Response> findAllStock() {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();

        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data retrieved !");


        response.setData(stockService.findAll());

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }


    @GetMapping(value = "/{id}")
    ResponseEntity<Response> getStockById(@Validated @PathVariable("id") Long stockId) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();

        response.setService("Controller method loc : " + methodName);
        response.setMessage("Successfully retrieve by Stock ID : " + stockId );

        response.setData(stockService.findById(stockId));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @PutMapping(value = "/{id}")
    ResponseEntity<Response> updateStock(@PathVariable("id") Long stockId, @RequestBody @Valid StockRespDto stockDto) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data with ID : " + stockId + " updated !");
        response.setData(stockService.update(stockId, stockDto));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @DeleteMapping(value = "/{id}")
    ResponseEntity<Response> deleteStockById(@Validated @PathVariable("id") Long stockId) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data with ID " + stockId + " deleted !");
        response.setData(stockService.findById(stockId));

        stockService.delete(stockId);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);

    }

}
