package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.PurchaseRespDto;
import com.taskapi.manufacture.service.PurchaseService;
import com.taskapi.manufacture.util.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * This is a Javadoc comment
 */
@RestController
@RequestMapping(value = "purchase")

public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;

    @PostMapping
    ResponseEntity<Response> addPurchase(@RequestBody @Valid PurchaseRespDto purchaseRespDto){

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Purchasing Success !");

        response.setData(purchaseService.create(purchaseRespDto));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @GetMapping
    ResponseEntity<Response> findAllPurchase() {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data retrieved !");
        response.setData(purchaseService.findAll());

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @GetMapping(value = "/{id}")
    ResponseEntity<Response> findPurchaseById(@PathVariable("id") Long purchaseId) {
        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Successfully retrieve by Purchase ID : " + purchaseId);
        response.setData(purchaseService.findById(purchaseId));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

}