package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.CustomerRespDto;
import com.taskapi.manufacture.service.CustomerService;
import com.taskapi.manufacture.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * This is a Javadoc comment
 */
@RestController
@RequestMapping(value = "customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @PostMapping
    ResponseEntity<Response> addCustomer(@RequestBody @Valid CustomerRespDto customerDto) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Add Customer Success !");
        response.setData(customerService.create(customerDto));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @GetMapping
    ResponseEntity<Response> findAllCustomer() {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data retrieved !");
        response.setData(customerService.findAll());

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @GetMapping(value = "/{id}")
    ResponseEntity<Response> findCustomerById(@PathVariable("id") Long customerId) {
        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Successfully retrieve by Customer ID : " + customerId );
        response.setData(customerService.findById(customerId));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @PutMapping(value = "/{id}")
    ResponseEntity<Response> updateCustomer(@PathVariable("id") Long customerId, @RequestBody @Validated CustomerRespDto customerDto) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data with ID : " + customerId + " updated !");
        response.setData(customerService.update(customerId, customerDto));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @DeleteMapping(value = "/{id}")
    ResponseEntity<Response> deleteCustomerById(@PathVariable("id") Long customerId) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data with ID " + customerId + " deleted !");
        response.setData(customerService.findById(customerId));

        customerService.delete(customerId);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);

    }
}

