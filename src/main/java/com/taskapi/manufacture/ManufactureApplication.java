package com.taskapi.manufacture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is a Javadoc comment
 */
@SpringBootApplication
public class ManufactureApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManufactureApplication.class, args);
	}

}
