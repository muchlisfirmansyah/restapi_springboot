package com.taskapi.manufacture.exception;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@XmlRootElement(name = "error")
public class ErrorResponse {

private String msg;
private List<String> details;
    /**
     * This is a Javadoc comment
     * @param msg, details
     */
    public ErrorResponse(String msg, List<String> details) {
        super();
        this.msg = msg;
        this.details = details;
    }
}
