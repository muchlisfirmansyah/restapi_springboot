package com.taskapi.manufacture.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This is a Javadoc comment
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class RecordNotFound extends RuntimeException{
    /**
     *
     * @param exception
     */
    public RecordNotFound(String exception){
        super(exception);
    }
}