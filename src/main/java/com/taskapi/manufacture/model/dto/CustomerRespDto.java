package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;


/**
 * Customer DTO
 */
@Data
@NoArgsConstructor
public class CustomerRespDto {

    @NotNull(message = "Name is a required")
    private String customerName;

    @NotNull(message = "Status is a required")
    private String status;

    @NotNull(message = "Account Balance is a required")
    @PositiveOrZero
    private Double accountBalance;

}
