package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

/**
 * This is a Javadoc comment
 */
@Data
@NoArgsConstructor
public class StockRespDto {

    @NotNull(message = "Name is a required")
    private String name;

    @NotNull(message = "Qty Stock is a required")
    @PositiveOrZero(message = "Negative Qty Stock is not allowed")
    private Integer qtyStock;

    @NotNull(message = "Measurement Unit is a required")
    private String measurementUnit;

    private String status;

    @NotNull(message = "Unit Price is a required")
    @PositiveOrZero(message = "Negative Unit Price is not allowed")
    private Double unitPrice;

    @NotNull(message = "Storage Location is a required")
    private String storageLocation;

}
