package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is a Javadoc comment
 */
@Data
@NoArgsConstructor
public class StockReqDto {

    private Long stockId;

    private String name;

    private Integer qtyStock;

    private String measurementUnit;

    private String status;

    private Double unitPrice;

    private String storageLocation;

}
