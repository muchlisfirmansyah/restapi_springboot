package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
@NoArgsConstructor
public class PurchaseReqDto {

    private Long purchaseId;

    private Long customerId;

    private Long stockId;

    private Integer qtyPurchase;

    private Double totalPrice;

    private Date createdDate = new Date();

}
