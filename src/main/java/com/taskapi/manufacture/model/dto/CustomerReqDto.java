package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Customer DTO
 */
@Data
@NoArgsConstructor
public class CustomerReqDto {

    private Long customerId;

    private String customerName;

    private String status;

    private Double accountBalance;

}
