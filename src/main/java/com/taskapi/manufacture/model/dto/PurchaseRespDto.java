package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.sql.Date;

/**
 * This is a Javadoc comment
 */
@Data
@NoArgsConstructor
public class PurchaseRespDto {

    @NotNull
    @NumberFormat
    private Long customerId;

    @NotNull
    private Long stockId;

    @NotNull
    @PositiveOrZero
    private Integer qtyPurchase;

    private Double totalPrice;

    @DateTimeFormat
    private Date createdDate;

}
