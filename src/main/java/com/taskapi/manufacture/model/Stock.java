package com.taskapi.manufacture.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@Entity
@Table(name = "STOCK")
public class Stock implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STOCK_ID")
    private Long stockId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "QTY_STOCK")
    private Integer qtyStock;

    @Column(name = "MEASURE_UNIT")
    private String measurementUnit;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "UNIT_PRICE")
    private Double unitPrice;

    @Column(name = "STORAGE_LOC")
    private String storageLocation;

}
