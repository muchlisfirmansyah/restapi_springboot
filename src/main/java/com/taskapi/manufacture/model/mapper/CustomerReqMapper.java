package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.dto.CustomerReqDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerReqMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Customer.class , CustomerReqDto.class)
                .byDefault()
                .register();
    }
}
