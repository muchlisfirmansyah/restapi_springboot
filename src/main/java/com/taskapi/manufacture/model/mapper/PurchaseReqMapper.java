package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.Purchase;
import com.taskapi.manufacture.model.dto.CustomerReqDto;
import com.taskapi.manufacture.model.dto.PurchaseReqDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class PurchaseReqMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Purchase.class , PurchaseReqDto.class)
                .byDefault()
                .register();
    }
}
