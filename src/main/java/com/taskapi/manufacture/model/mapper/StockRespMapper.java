package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Stock;
import com.taskapi.manufacture.model.dto.StockRespDto;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class StockRespMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(StockRespDto.class, Stock.class)
                .byDefault()
                .exclude("stockId")
                .register();
    }
}
