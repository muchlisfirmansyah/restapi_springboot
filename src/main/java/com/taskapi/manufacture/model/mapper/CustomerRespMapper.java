package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.dto.CustomerReqDto;
import com.taskapi.manufacture.model.dto.CustomerRespDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerRespMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(CustomerRespDto.class, Customer.class )
                .byDefault()
                .exclude("customerId")
                .register();
    }
}
