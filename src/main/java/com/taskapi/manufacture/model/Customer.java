package com.taskapi.manufacture.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@Entity
@Table(name = "Customer")

public class Customer implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    @Column(name = "CUSTOMER_NAME")
    private String customerName;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "ACCOUNT_BALANCE")
    private Double accountBalance;

}
